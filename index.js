var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var port = process.env.PORT || 3000;
//var sys = require('sys')
var exec = require('child_process').exec;
var util = require('util');
// var sleep = require('sleep');

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});
var msgs = "";
var sum = "";
io.on('connection', function(socket){

  socket.on('chat message', function(msg){
      io.emit('chat message', msg);

    //io.emit('chat message', msgs);
//function puts(error, stdout, stderr) { sys.puts(stdout) }
    
    //exec = require('child_process').exec,
    //child;
    //   if (msg === 'summary') {
    //       io.emit('chat message', 'Please be patient while we summarize your discussion!');
    //   }
    if (msg === 'collect') {
        exec('head -1 decode' + file + '/decode*', // command line argument directly in string
            function (error, stdout, stderr) {      // one easy function to capture data/errors
                console.log('stdout: ' + stdout);
                sum = stdout.toString();

                console.log('stderr: ' + stderr);
                if (error !== null) {
                    console.log('exec error: ' + error);
                }
            });
        io.emit('chat message', sum);
    }
	if (msg === 'summarize') {
		//io.emit('chat message', 'now summarizing');
		//io.emit('chat message', msgs);
        console.log(msgs);

        file = Math.floor(Date.now());

		// construct binary file
        exec('python binarize.py --output_file binary' + file + ' --messages "' + msgs + '"',
            function (error, stdout, stderr) {
                console.log('stdout: ' + stdout);
                console.log('stderr');
                if (error !== null) {
                    console.log('exec error: ' + error);
                }
            });


		// Run decode and wait for new file
        exec('timeout 600s ../models/bazel-bin/textsum/seq2seq_attention --mode=decode --article_key=article --abstract_key=abstract ' +
			'--data_path=binary' + file + ' --vocab_path=model/cnn-vocab --log_root=model/log_root_cnn ' +
			'--decode_dir=decode' + file + ' --beam_size=2 &',
            function (error, stdout, stderr) {
                console.log('stdout: ' + stdout);
                console.log('stderr');
                if (error !== null) {
                    console.log('exec error: ' + error);
                }
            });


		// busy wait for new response
		// var finished = false;
		// while (!finished) {
         //    exec('ls decode' + file + '/ -Art | tail -n 1',
         //        function (error, stdout, stderr) {
         //            var sleep = require('sleep');
         //            sleep.sleep(10);
         //            //console.log('stdout: ' + stdout);
		// 			console.log('stdout: ' + stdout);
		// 			if (stdout.toString().length > 1) {
		// 				finished = true;
		// 			}
		// 			console.log('stderr');
		// 			if (error !== null) {
		// 				console.log('exec error: ' + error);
		// 			}
         //        });
		// }

           // sleep.sleep(60);

        //var waitTill = new Date(new Date().getTime() + 180 * 1000);
        //while(waitTill > new Date()){}
        //
		// Print response

        // sum = 'yes';
        // $('#messages').append($('<li>').text(sum));
        // waitTill = new Date(new Date().getTime() + 10 * 1000);
        // while(waitTill > new Date()){}

        // socket.connect();
        // io.socket.reconnect();

        // io.emit('summary', sum);


	} else {

        msgs = msgs + msg;
    }
	// io.emit('chat message', sum);
  });

    socket.on('summary', function(msg){
        io.emit('summary', sum);
    });

});


//exec("ls -la", puts);

// io.on('connection', function(socket){
//   socket.on('summarize', function(){
//     io.emit('summarize');
//   });
// });

http.listen(port, function(){
  console.log('listening on *:' + port);
});
