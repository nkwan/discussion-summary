import collections
import struct
import sys

from os import listdir
from os.path import isfile, join

from nltk.tokenize import sent_tokenize

import tensorflow as tf
from tensorflow.core.example import example_pb2

from numpy.random import seed as random_seed
from numpy.random import shuffle as random_shuffle

random_seed(123)  # Reproducibility

FLAGS = tf.app.flags.FLAGS
#tf.app.flags.DEFINE_string('', 'text_to_binary',
#                           'Either text_to_vocabulary or text_to_binary.'
#                           'Specify FLAGS.in_directories accordingly.')
#tf.app.flags.DEFINE_string('in_directories', '', 'path to directory')
tf.app.flags.DEFINE_string('output_file', '', 'comma separated paths to files')
tf.app.flags.DEFINE_string('messages', '', 'messages')
#tf.app.flags.DEFINE_string('split', '', 'comma separated fractions of data')


messages = FLAGS.messages
output_file = FLAGS.output_file


with open(output_file, 'wb') as writer:

    title = '<d><p><s>some random title that needs to be here</s></p></d>'

    body = messages.decode('utf8').replace('\n', ' ').replace('\t', ' ')
    sentences = sent_tokenize(body)
    body = '<d><p>' + ' '.join(['<s>' + sentence + '</s>' for sentence in sentences]) + '</p></d>'
    body = body.encode('utf8')

    tf_example = example_pb2.Example()
    tf_example.features.feature['article'].bytes_list.value.extend([body])
    tf_example.features.feature['abstract'].bytes_list.value.extend([title])
    tf_example_str = tf_example.SerializeToString()
    str_len = len(tf_example_str)
    writer.write(struct.pack('q', str_len))
    writer.write(struct.pack('%ds' % str_len, tf_example_str))

    #
    # tf_example = example_pb2.Example()
    # for feature in inp.strip().split('\t'):
    #     (k, v) = feature.split('=')
    #     tf_example.features.feature[k].bytes_list.value.extend([v])
    # tf_example_str = tf_example.SerializeToString()
    # str_len = len(tf_example_str)
    # writer.write(struct.pack('q', str_len))
    # writer.write(struct.pack('%ds' % str_len, tf_example_str))
writer.close()